## code to prepare `help_tab` dataset goes here

# hjälptabell
help_tab <- data.frame(
  name = c("Remissankomst till första besök",
           "Andel beslut på multidisciplinär konferens"),
  ind = c("ind1",
          "ind2"),
  ledtid = c("ind1_ledtid",
             NA),
  period = c("a_besok",
             "a_besldat"),
  sjukhus = rep("a_anmsjh", 2),
  period_label = c("Besöksdatum",
                   "Datum för behandlingsbeslut"),
  l1 =  c(50, 90),
  l2 =  c(80, 95),
  description = c("Andel patienter där antalet (vecko)dagar från remissankomst (enligt blankett 1) till  första besök på utredande ÖNH-mottagning (enligt blankett 1) uppgår till maximalt 5 dagar (negativa ledtider, ledtider längre än ett år och patienter ej handlagda på ÖNH-klinik är exkluderade). Sjukhusredovisningen är efter anmälande sjukhus för anmälningsblanketten, region visar den region patienten var folkbokförd i vid diagnos. Perioden är datum för första besök.",
                  "Andel patienter för vilka behandlingsbeslut tagits vid multidiciplinär konferens (enligt blankett 1). (Patienter med läppcancer och patienter ej handlagda på ÖNH-klinik är exkluderade). Sjukhusredovisningen är efter anmälande sjukhus för anmälningsblanketten, region visar den region patienten var folkbokförd i vid diagnos. Perioden är datum för behandlingsbeslut."))


usethis::use_data(help_tab, overwrite = TRUE)
