#' @keywords internal
"_PACKAGE"

utils::globalVariables(c("ind_ledtid", "ledtid", "region_namn", "region",
                         "ind", "value", "sjukhus", "ind_antal", "ind_txt",
                         "RegionName", "region", "region_namn", "sjhregion",
                         "sjukhus2", "value", "Indikator_hidden", "ind_perc",
                         "PosLevel", "ParentPosCode", "ParentPosName", "name",
                         ".", "Indikator", "drawCI"))

# The following block is used by usethis to automatically manage
# roxygen namespace tags. Modify with care!
## usethis namespace: start
## usethis namespace: end
NULL
